SOURCE_DIRECTORY=src/
BUILD_DIRECTORY=build/
DOCUMENTATION_DIRECTORY=docs/

MAKE_SUCCESS=@echo -e "\e[1;32mSUCCESS:\e[0m"
MAKE_ERROR=@echo -e "\e[1;31mERROR:\e[0m"

build: clean check_dependencies setup
	./compile.sh "$(SOURCE_DIRECTORY)" "$(BUILD_DIRECTORY)"
	node apply_config.js $(SOURCE_DIRECTORY) $(BUILD_DIRECTORY) $(CONFIG)

assets: clean_assets check_dependencies setup_assets
	./compile.sh "$(SOURCE_DIRECTORY)asset" "$(BUILD_DIRECTORY)asset"

documentation:
	rm -rf $(DOCUMENTATION_DIRECTORY)
	mkdir -p $(DOCUMENTATION_DIRECTORY)
	@./check_command_dependency.sh jsdoc
	jsdoc $(SOURCE_DIRECTORY) -r -d $(DOCUMENTATION_DIRECTORY)

setup:
	mkdir -p $(BUILD_DIRECTORY)

setup_assets:
	mkdir -p $(BUILD_DIRECTORY)asset

check_dependencies:
	@./check_command_dependency.sh sass
	@./check_command_dependency.sh node
	@./check_command_dependency.sh uglifyjs

clean:
	rm -rf $(BUILD_DIRECTORY)
	rm -rf .sass-cache

clean_assets:
	rm -rf $(BUILD_DIRECTORY)asset
	rm -rf .sass-cache
