-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.43-0ubuntu0.14.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table frost.files
CREATE TABLE IF NOT EXISTS `files` (
  `gID` int(255) NOT NULL COMMENT 'global id',
  `uID` int(100) NOT NULL,
  `location` int(100) NOT NULL,
  `mime` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hashName` varchar(20) CHARACTER SET latin1 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `size` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `dateIntroduced` datetime NOT NULL,
  `dateLastUpdated` datetime NOT NULL,
  `previousLocation` int(100) NOT NULL DEFAULT '1',
  `dateLastRelocated` datetime NOT NULL,
  `lastValidated` datetime NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `iconLastGenerated` datetime NOT NULL,
  PRIMARY KEY (`gID`),
  UNIQUE KEY `hashName` (`hashName`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
