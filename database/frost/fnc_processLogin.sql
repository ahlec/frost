-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.43-0ubuntu0.14.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for function frost.fnc_processLogin
DELIMITER //
CREATE DEFINER=`frost_admin`@`localhost` FUNCTION `fnc_processLogin`(`usrname` VARCHAR(50) CHARSET utf8, `pwd` VARCHAR(32)) RETURNS varchar(50) CHARSET utf8
    NO SQL
BEGIN
	IF (SELECT 1 FROM users x WHERE x.username = @usrname AND x.password = @pwd) <> 1 THEN
		RETURN 0;
	ELSE
		UPDATE
			users x
		SET
			x.lastLogin = NOW()
		WHERE
			x.username = usrname AND
			x.password = pwd;
		RETURN (SELECT uHash from users x WHERE x.username = usrname AND x.password = pwd LIMIT 1);
	END IF;
END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
