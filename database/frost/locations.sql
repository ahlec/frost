-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.43-0ubuntu0.14.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table frost.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `location_id` int(100) NOT NULL AUTO_INCREMENT,
  `gID` int(255) NOT NULL COMMENT 'global ID',
  `uID` int(10) NOT NULL,
  `handle` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `parent_location` int(100) NOT NULL,
  `type` enum('Folder','Special') CHARACTER SET latin1 NOT NULL,
  `custom_icon` varchar(100) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `previousLocation` int(100) NOT NULL,
  `dateLastRelocated` datetime NOT NULL,
  `contentsContextHandleAdditions` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
