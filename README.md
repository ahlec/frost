# frOSt #

### Building ###
Despite all frOSt being written in interpretted languages, frOSt has a build compilation process. During this process, Sass code is translated to CSS, Javascript is uglified, and configuration properties are applied as hard-coded values into config files (to name a few operations). By default, frOSt is built to a directory in the root of the project under build/.

The first step to building frOSt is to put together the configuration file. This is the file that specifies all of the necessary variable configurations for the instance of frOSt that is being built for (for instance, database connection information or the web path to where frOSt will be located). This is stored as a JSON file in the root of the project. By default, the compilation process will look for a "config.json" file, but it is possible to override that:

    make                            # will use "config.json" as no alternative was provided
    make CONFIG=my_site.json"       # will use "my_site.json" for configuration properties
    
The build process is meant to be absolute, meaning that once it is (successfully) finished, the build should be able to be deployed to any server that is hosting it and have it run immediately (NOTE: this obviously does not take into account the operations to create or update the database, which is a process that must be done separately.

Given frequency, there is an additional build specification for building just the assets. This process will compile the assets directory, leaving in place all other files in the build directory. Note that, because of this, you'll need to have run the full build sequence in order to have the rest of the files present as well.

    make assets
    
As per expected standard, there are the expected operations as well:

    make clean
    
### Documentation ###
This iteration of frOSt includes heavy documentation in the code (presently only for JavaScript). Compiling the documentation is very simple:

    make documentation

This will create all necessary documentation files in a docs/ directory under the root of the project.

### Required Programs ###
The following programs are required in order to build frOSt, along with the minimum version tested to work:
* [Sass 3.4](http://sass-lang.com/)
* [Node.js 0.12.5](https://nodejs.org/)
* [UglifyJS 2](https://github.com/mishoo/UglifyJS2)
