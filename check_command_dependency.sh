#!/bin/sh

{
    which $1
} &> /dev/null
if [ $? == 0 ]; then
    echo -e "[dependency] $1... \e[1;32mINSTALLED\e[0m"
else
    echo -e "[depdenency] $1... \e[1;31mNOT INSTALLED\e[0m"
    exit 1
fi