<?php
require_once ("config.inc.php");

/// --------------------------------------- SESSION hashing (all the necessary stuff, since this needs to be hardcoded into the HTML)
require_once (FROST_DOCUMENT_ROOT . "/server/security/init.inc.php");
require_once (FROST_DOCUMENT_ROOT . "/server/security/renewSessionHash.php");
renewSessionHash();

function outputAsset($assetFile, $assetType)
{
	if ($assetType == "js")
	{
		echo "		<script type=\"text/javascript\" src=\"./asset/", $assetFile,
			"\"></script>\n";
	}
	else
	{
		echo "		<link rel=\"stylesheet\" href=\"./asset/", $assetFile,
			"\" />\n";
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>frOSt</title>
		<base href="<?php echo FROST_WEB_PATH; ?>/" />
		<link href='http://fonts.googleapis.com/css?family=Raleway:600,400' rel='stylesheet' type='text/css' />
<?php require_once("asset/assets.inc.php"); ?>
        <script>
            $(document).ready(function () {
                Security.start("<?php echo $_SESSION[FROST_SESSION_HASH]["hash"]; ?>");
            });
        </script>
    </head>
    <body>
    </body>
</html>