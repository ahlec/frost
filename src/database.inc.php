<?php
if (!class_exists("FrostDatabase"))
{
    class FrostDatabase
    {
        private $_database;
        private $_lastErrorNo;
        private $_lastErrorMessage;
        
        function __construct($server, $username, $password, $schema)
        {
            $this->_database = new mysqli($server, $username, $password, $schema);
            if ($this->_database->connect_error)
            {
                $this->_database = null;
                $this->_lastErrorNo = $this->_database->connect_errno;
                $this->_lastErrorMessage = $this->_database->connect_error;
            }
            else if (mysqli_connect_error())
            {
                $this->_database = null;
                $this->_lastErrorNo = mysqli_connect_errno();
                $this->_lastErrorMessage = mysqli_connect_error();
            }
            
            if ($this->isValid())
            {
                $setup_results = $this->query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'; " .
                                              "PRAGMA short_column_names = ON; PRAGMA encoding = \"UTF-8\";");
                if ($setup_results === false)
                {
                    $this->_database->close();
                    $this->_database = null;
                    $this->_lastErrorMessage = -17;
                    $this->_lastErrorMessage = "Failed to perform initial FrostDatabase setup";
                }
            }
        }
        
        function isValid()
        {
            return ($this->_database !== null);
        }
        
        function query($query)
        {
            $queryResults = $this->_database->multi_query($query);
            if ($queryResults === false)
            {
                $this->_lastErrorNo = $this->_database->errno;
                $this->_lastErrorMessage = $this->_database->error;
                return false;
            }
            
            return new FrostQueryResults($this->_database);
        }
        
        function querySingle($query, $returnFullRow = false)
        {
            $fullQueryResults = $this->query($query);
            
            if ($fullQueryResults === false)
            {
                // Query failed
                return false;
            }
            
            $fullQueryResults->makeSingleQueryResults();
            return $fullQueryResults;
        }
        
        function escapeString($string)
        {
            return $this->_database->real_escape_string($string);
        }
        
        function close()
        {
            $this->_database->close();
            $this->_database = null;
        }
        
        function getLastErrorCode()
        {
            return $this->_lastErrorNo;
        }
        
        function getLastErrorMessage()
        {
            return $this->_lastErrorMessage;
        }
        
        function getLastAutoIncrement()
        {
            return $this->_database->insert_id;
        }
    }

    class FrostQueryResults
    {
        private $_results = array();    // Array of arrays, where each sub-array is a result set
                                        // (ie, this is an array of multi_query results, though it's
                                        // standardised so that it and single query results have the same
                                        // pathway).
        private $_isSingleQueryResults; // Will allow us to selectively deal with $_results to limit
                                        // the results to only the first row of the first result set
        private $_hasIteratedSingleQuery;   // A boolean flag representing if the single query results row has been
                                            // displayed, since it's just an array and not a mysqli object
        
        // The way that this functions is that:
        //      - If it's a single query, then $_results is an array with a single item -- the ASSOC array of the first
        //        row retrieved from the first result set. There isn't a guarantee that $_results will have an item (ie,
        //        can be empty) but there is a guarantee that it will never have a length of 2+
        //      - If it's not a single query, then we are going to treat it like a multi result set. If there is only one
        //        result set, then $_results will be an array of a single mysqli results. This class is able to reset the
        //        current iterator to the current result set (ie, can reset to row 0 of the current result set) but you are
        //        not able to return to an earlier result set. As such, in these instances, when we advance to the next result
        //        set, we actually free and get rid of the result set at the beginning of $_results, so that the next result
        //        set is now at index 0. As such, most all calls here will always use index of 0, as that's the current result
        //        set.
        
        
        function __construct($database)
        {
            do
            {
                $result = $database->store_result();
                if (is_object($result))
                {
                    $this->_results[] = $result;
                }
            } while ($database->next_result());
            $this->_isSingleQueryResults = false;
            $this->_hasIteratedSingleQuery = false;
        }
        
        function makeSingleQueryResults()
        {
            // Get the first row of the first result set and replace $_results with that particular array
            // (Note that it shouldn't be a nested array
            if (count($this->_results) > 0)
            {
                // Claim the first result set and free all subsequent ones. We can modify the $_results array
                // directly because we're going to create a new one in just a minute.
                $firstResultSet = array_shift($this->_results);
                while (count($this->_results) > 0)
                {
                    array_shift($this->_results)->free();
                }
                
                // Get the first row from our saved first result set
                if (is_object($firstResultSet))
                {
                    $firstRow = $firstResultSet->fetch_array();
                    if ($firstRow !== null)
                    {
                        $this->_results = array($firstRow);
                    }
                    else
                    {
                        $this->_results = array();
                    }
                    $firstResultSet->free();
                }
            }
            else
            {
                $this->_results = array();
            }            
            
            $this->_isSingleQueryResults = true;
            $this->_hasIteratedSingleQuery = false;
            
            if (is_object($this->_results[0]))
            {
                $this->_results[0]->data_seek(0);
                $this->_results[0] = $this->_results[0]->fetch_array();
            }
        }
        
        function fetchArray()
        {
            if ($this->_isSingleQueryResults)
            {
                if ($this->_hasIteratedSingleQuery)
                {
                    return null;
                }
                else
                {
                    $this->_hasIteratedSingleQuery = true;
                }
                
                if (count($this->_results) < 1)
                {
                    return null;
                }
                return $this->_results[0];
            }
            
            return $this->_results[0]->fetch_array();
        }
        
        function nextResultSet()
        {
            if (count($this->_results) == 0)
            {
                return false;
            }
            
            if (is_object($this->_results[0]))
            {
                $this->_results[0]->free();
            }
            array_shift($this->_results);
            return true;
        }
        
        function resetResultSet()
        {
            if (count($this->_results) == 0)
            {
                return;
            }
            
            if (is_object($this->_results[0]))
            {
                $this->_results[0]->data_seek(0);
            }
            
            $this->_hasIteratedSingleQuery = false;
        }
        
        function getNumberOfRows()
        {
            if ($this->_isSingleQueryResults)
            {
                // It'll either be 0 or 1
                return count($this->_results);
            }
            
            return $this->_results[0]->num_rows;
        }
    }

}
?>
