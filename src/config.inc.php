<?php
// Core
define ("FROST_WEB_PATH", "{{{CONFIG:WebPath}}}");           // No trailing slash
define ("FROST_DOCUMENT_ROOT", "{{{CONFIG:DocumentRoot}}}"); // No trailing slash

// Session
session_name("some_name");
define ("FROST_AUTH_SESSION_NAME", "frOSt_auth_session");
define ("FROST_SESSION_HASH", "frOSt_session_hash");
define ("FROST_SESSION_LIFETIME_DURATION", 3600); // measured in seconds; after this amount of time, a session is thrown away
                                                  // DO NOT MAKE THIS LESS THAN FIVE MINUTES!!! (The Javascript will attempt to renew
                                                  // every five minutes)
session_start();

// Database
require_once (FROST_DOCUMENT_ROOT . "/database.inc.php");
function openDatabase()
{
    return new FrostDatabase("{{{CONFIG:Database.Host}}}", "{{{CONFIG:Database.User}}}",
        "{{{CONFIG:Database.Password}}}", "{{{CONFIG:Database.Schema}}}");
}

// Parameters
require_once (dirname(__FILE__) . "/config/parameters.inc.php");
?>