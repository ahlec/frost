<?php

require_once("./config.inc.php");
require_once("server/auth/FrostUser.inc.php");

$database = openDatabase();
if (!$database->isValid())
{
    die("invalid database");
}

$results = $database->query("SELECT * FROM user");
if ($results === false)
{
    die("Error from query");
}

echo "<pre>";
if ($row = $results->fetchArray())
{
    print_r($row);
}
echo "</pre>";

if (isset($_GET["userid"])) {
    $user = FrostUser::get(openDatabase(), $_GET["userid"]);
    print_r($user);
}

?>