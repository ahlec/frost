<?php
if (!function_exists("outputAsset"))
{
	function outputAsset($assetFile, $assetType)
	{
		// Do nothing
	}
}

// JS
outputAsset("js/jquery-2.0.3.min.js", "js");
outputAsset("js/jquery-ui-1.10.3.custom.min.js", "js");
outputAsset("js/oop.js", "js");

// Core
outputAsset("core/Exception.js", "js");
outputAsset("core/Util.js", "js");
outputAsset("core/Ajax.js", "js");
outputAsset("core/Reflection.js", "js");
outputAsset("core/Form.js", "js");
outputAsset("core/ServerFunction.js", "js");
outputAsset("core/Workspace.js", "js");
outputAsset("core/core.css", "css");
outputAsset("core/Pane.js", "js");
outputAsset("core/Frame.js", "js");
outputAsset("core/Application.js", "js");
outputAsset("core/Entity.js", "js");
outputAsset("core/Directory.js", "js");
outputAsset("core/aes.js", "js");

// Modules
outputAsset("server/Auth.js", "js");
outputAsset("server/Security.js", "js");

// Applications
outputAsset("applications/login/main.js", "js");
outputAsset("applications/login/main.css", "css");
outputAsset("applications/scribe/scribe.js", "js");

// TEMP!!! (theming will be dynamic later, but for now we're going to hardcode it)
outputAsset("themes/legacy/theme.css", "css");

?>