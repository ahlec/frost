/* Security */
Security = (function () {
    var RENEWAL_INTERVAL_DELAY_IN_MILLISECONDS = 300000, // five minutes
        m_sessionHash,
        m_renewalInterval,
        m_renewSessionFunction = ServerFunction("security/renewSessionHash.php", false);
    
    return {
        /** State Management **/
        start: function (sessionHash) {
            m_sessionHash = sessionHash;
            m_renewalInterval = setInterval(function () {
                Security.renewSessionHash()
            }, RENEWAL_INTERVAL_DELAY_IN_MILLISECONDS);
        },
        
        renewSessionHash: function () {
            var renewResult = m_renewSessionFunction();
            if (renewResult === false) {
                clearTimeout(m_renewalInterval);
                alert("YOUR SESSION HAS EXPIRED AND YOU NEED TO HARD REFRESH THE PAGE!!!");
            }
        },
        
        /** SESSION hash **/
        decryptSession: function (encrypted) {
            return CryptoJS.AES.decrypt(encrypted, m_sessionHash);
        },
        encryptSession: function (message) {
            return CryptoJS.AES.encrypt(message, m_sessionHash);
        }
    };
})();