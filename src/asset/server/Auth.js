/* AUTH */
Auth = {}
Auth.isLoggedIn = ServerFunction("auth/isLoggedIn.php", false, ["reset"]);
Auth.login = ServerFunction("auth/login.php", false, ["username", "password"]);

/* LOGIN WINDOW */
function LoginWindow() {
	var form = new Form(Auth.login);
	
	$(document.createElement("input")).attr("name", "username").appendTo(form);
	$(document.createElement("input")).attr("type", "password").attr("name", "password").appendTo(form);
	$(document.createElement("input")).attr("type", "submit").val("Submit").appendTo(form);
	
	form.process(function (result) {
		console.log('form result processing!');
		console.dir(result);
	});
	
	form.appendTo($(document.body));
};