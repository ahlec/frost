(function () {
	extend = function(baseClass)
	{
		function instantiator(instantiateArgs) {
			return baseClass.apply(this, instantiateArgs);
		};
		instantiator.prototype = baseClass.prototype;
		
		var parentInstance = new instantiator(Array.prototype.splice.call(arguments, 1));
		
		for (var parentMethod in parentInstance) {
			this[parentMethod] = parentInstance[parentMethod].bind(parentInstance);
		}
		
		this.base = parentInstance;
	}
})();

OOP = {};
OOP.inherits = function (object, classFunc) {
	if (typeof object != "object") {
		return false;
	}
	
	// Directly?
	if (object instanceof classFunc) {
		return true;
	}
	if (object.hasOwnProperty("isPublicFor") &&
	    (object.isPublicFor instanceof classFunc || object.isPublicFor == classFunc)) {
		return true;
	}
	
	// Doesn't inherit
	return false;
};