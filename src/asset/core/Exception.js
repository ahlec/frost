function Exception(message, name) {
	name = (name || "Generic Exception");
	message = (message || "An unspecified exception occurred during execution.");
	var dataElements = new Array();

	this.toString = function() {
		return name + ": " + message;
	}
};