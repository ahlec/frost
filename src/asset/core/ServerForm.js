function ServerForm() {
	var formElement = $(document.createElement("form")),
	    serverFunc = ServerFunction.call(null, arguments);

	formElement.submit(function (e) {
		e.preventDefault();
		console.dir("form submitted");
		serverFunc();
	});

	return formElement;
}