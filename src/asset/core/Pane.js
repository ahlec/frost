function Pane(frame, paneElem) {

	var panePublic = {
		getFrame: function() {
			return frame;
		},
		
		clear: function() {
			paneElem.empty();
			return panePublic;
		},
		
		append: function() {
            var elem;
            
            for (var index = 0; index < arguments.length; ++index) {
                elem = arguments[index];
                if (typeof elem == "object" && elem.hasOwnProperty("domElement")) {
                    elem = elem.domElement;
                }

                if (Util.isDomElement(elem)) {
                    paneElem.append(elem);
                }
            }
			
			return panePublic;
		},
		
		remove: function(elem) {
			if (typeof elem == "object" && elem.hasOwnProperty("domElement")) {
				elem = elem.domElement;
			}

			if (!Util.isDomElement(elem)) {
				throw new Exception("Move provide a valid DOM element to remove from the Pane.");
			}
			
			elem.remove();
		},
	};
	
	return panePublic;
};