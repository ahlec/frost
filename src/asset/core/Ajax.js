function Ajax(settings) {
	// Input validation
	if (typeof settings == "undefined") {
		throw new Exception("Must provide settings to use Ajax.");
	}
	
	if (typeof settings == "object") {
		if (typeof settings["url"] != "string") {
			throw new Exception("If Ajax is called with an object, there must be an element 'url' that has a string value.");
		}
	}

	// Create and return the object
	var inputUrl = (typeof settings == "object" ? settings["url"] : settings),
	    isAsync = (typeof settings == "object" ? (settings["isAsync"] === true) : false),
	    dataArray = {},
	    successCallbacks = [],
	    failureCallbacks = [];
		
	function buildErrorData(rawData) {
		var data = {
			errorCode: rawData["status"]
		};
		
		return data;
	}
	
	function onSuccess(rawData) {
		rawData = $.parseJSON(rawData);
		if (rawData["status"] == "error") {
			onFailure(rawData);
			return;
		}
		
		var data = ("data" in rawData ? rawData["data"] : null);
		for (var index = 0; index < successCallbacks.length; ++index) {
			successCallbacks[index](data);
		}
	}

	function onFailure(rawData) {
		var data = buildErrorData(rawData);
		for (var index = 0; index < failureCallbacks.length; ++index) {
			failureCallbacks[index](data);
		}
	}

	return {
		data: function (key, value) {
			if (typeof key != "string") {
				throw new Exception("Must provide a string for the data key name.");
			}
			key = key.trim();
			if (key.length == 0) {
				throw new Exception("Must provide a nonzero length string that is composed of more than just whitespace.");
			}
			if (typeof dataArray[key] != "undefined") {
				dataArray[key].push(value);
			}
			else {
				dataArray[key] = [value];
			}
			return this;
		},
		call: function () {
			$.ajax({
				url: inputUrl,
				data: dataArray,
				async: isAsync,
				success: onSuccess,
				error: onFailure
			});
		},
		success: function (callback) {
			if (typeof callback != "function") {
				throw new Exception("Provided callback must be a function.");
			}
			successCallbacks.push(callback);
			return this;
		},
		failure: function (callback) {
			if (typeof callback != "function") {
				throw new Exception("Provided callback must be a function.");
			}
			failureCallbacks.push(callback);
			return this;
		}
	};
}