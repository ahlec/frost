Util = {};
/* DOM */
Util.isDomElement = function (item) {
	if (typeof item != "object") {
		return false;
	}
	
	if (typeof item.jquery != "undefined") {
		item = item[0];
	}
	
	try {
		return item instanceof HTMLElement;
	}
	catch (e) {
		return (item.nodeType === 1 && typeof item.style == "object" && typeof item.ownerDocument == "object");
	}
};
Util.getImmediateSelector = function (elem) {
	if (!Util.isDomElement(elem)) {
		throw new Exception("Must provide a valid DOM element in order to get the immediate selector.");
	}
	
	elem = $(elem);
	
	var tagName = elem.prop("tagName").toLowerCase();
	if (elem.get(0) == document.documentElement) {
		return tagName;
	}
	
	var idProp = elem.attr("id");
	if (typeof idProp != "undefined" && idProp.length > 0) {
		return tagName + "#" + idProp;
	}
	
	var currentSelector = tagName;
		classNames = elem.prop("className");
	if (typeof classNames != "undefined" && classNames.length > 0) {
		currentSelector += "." + classNames.replace(" ", ".");
	}
	
	var allSelectorChildren = elem.parent().children(currentSelector);
	if (allSelectorChildren.length == 1) {
		return currentSelector;
	}
	
	currentSelector += ":nth-child(" + (allSelectorChildren.index(elem) + 1) + ")";
	return currentSelector;
}
Util.getSelector = function (elem) {
	if (!Util.isDomElement(elem)) {
		throw new Exception("Must provide a valid DOM element in order to get the selector.");
	}
	
	elem = $(elem);
	var pieces = [Util.getImmediateSelector(elem)],
		currentParent = elem.parent();
	
	while (currentParent.length == 1) {
		if (currentParent[0] == document) {
			break;
		}
		
		pieces.push(Util.getImmediateSelector(currentParent[0]));
		currentParent = currentParent.parent();
	}
	
	return pieces.reverse().join(" > ");
};
Util.newDomId = function () {
	var currentId = "";
	do {
		currentId += "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789"[Math.floor(Math.random() * 53)];
	} while ($("#" + currentId).length > 0);
	
	return currentId;
};

/* STRINGS */
Util.standardizeStringEntities = function (str) {
	if (typeof str != "string") {
		return "";
	}
	
	var matches = str.match(/&(#[0-9]+|#x[0-9abcdef]+|\w+);/gi);
	
	if (matches == null) {
		return str;
	}
	
	console.dir(matches);
	var currentCodepoint = null,
		replaceRegex = null;
	for (var index = 0; index < matches.length; ++index) {
		if (/^&#x[0-9abcdef]+;$/i.test(matches[index])) {
			currentCodepoint = parseInt("0x" + matches[index].substring(3, matches[index].length - 1));
		}
		else if (/^&#[0-9]+;$/.test(matches[index])) {
			currentCodepoint = parseInt(matches[index].substring(2, matches[index].length - 1));
		}
		else {
			continue;
		}
		
		if (currentCodepoint == 60 || // <
			currentCodepoint == 62) { // >
			continue;
		}
		
		replaceRegex = new RegExp(matches[index], "gi");
		str = str.replace(replaceRegex, String.fromCharCode(currentCodepoint));
	}
	return str;
}
