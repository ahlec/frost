var EntityManager = (function() {
	var cache = {};
	
	return {
		load: function (guid) {
			if (typeof guid === undefined) {
				throw new Exception("GUID must be provided in order to load from the EntityManager.");
			}
		},
	};
})();