function ServerFunction(path, failureReturnValue, parameterNames) {
	// Input validation
	if (typeof path == "undefined") {
		throw new Exception("Must provide a server path for a server function.");
	}
	parameterNames = (parameterNames || new Array());
	
	function servFuncIntCallbackFunc() {
		var ajax = new Ajax("./server/" + path),
			data = null;
		
		if (arguments.length > 0) {
			for (var index = 0; index < Math.min(parameterNames.length, arguments.length); ++index) {
				ajax.data(parameterNames[index], arguments[index]);
			}
		}
		ajax.success(function (returnData) {
			data = returnData;
		}).failure(function (returnData) {
			data = failureReturnValue;
		}).call();
		
		return data;
	};
	
	servFuncIntCallbackFunc.paramNames = parameterNames;
	
	return servFuncIntCallbackFunc;
}