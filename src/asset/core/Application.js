/** @namespace Core **/

/**
 * @name Core.ApplicationInitializer
 * @function
 * @description A function callback which will be called when a new instance of the
 * [Application]{@link Core.Application} is opened.
 * @public
 * @param {...*} [args] - A variable length series of values which can be used to initialize this instance of the
 * Application.
 * @returns {Core.Frame} The frame that represents the main window for this new instance of the Application.
 * @see {@link Core.ApplicationSettings}
 */

/**
 * Settings which are used when creating a new application (a new {@link Application} object,
 * not a new instance of an Application).
 * @public
 * @typedef {Object} Core.ApplicationSettings
 * @see Core.Application
 * @property {boolean} [isSingleInstance] - Indicates whether this application should be a singleton application or not.
 * A singleton application is an application that may only have one instance open at a time; attempting to open another
 * instance of a singleton application will only result in the current instance gaining focus. Default is 'false'.
 * @property {Core.ApplicationInitializer} init - The function which will be called as the entry point into a new
 * instance of the Application.
 */

/**
 * Defines a new application.
 * @param {Core.ApplicationSettings} settings - The settings used to define this Application.
 * @constructor
 * @class
 * @classdesc An Application is simply that -- an application. Applications are any program which runs with a frame.
 * Applications are meant to be reusable, and define a series of methods which allows for creating instances of the
 * Application. These instances are the actual frames
 * @public
 * @name Core.Application
 */
function Application(settings) {
    if (typeof settings !== "object") {
        throw new Exception("Creating a new Application requires settings to be provided.");
    }

    var m_instances = [],
        m_isSingleInstance = (settings.hasOwnProperty("isSingleInstance") ? settings.isSingleInstance === true : false),
        m_initFunction,
        m_publicApp;

    if (!settings.hasOwnProperty("init") || typeof settings.init !== "function") {
        throw new Exception("All Applications must define the init setting as a function.");
    }
    m_initFunction = settings.init;

    m_publicApp = {

        /**
         * Opens a new instance of this application. If this application is declared as a singleton application (an
         * application where only one instance may be open at a time) and there is already an instance open, then
         * that instance of the application will receive focus. Note that in this situation, the optional arguments
         * will not be passed to the instance of the application, as it is already open.
         * @public
         * @instance
         * @memberof Core.Application
         * @param {*} [args] Any arguments which should be passed to the initialiser of the application.
         */
        call: function (args) {
            if (m_isSingleInstance && m_instances.length > 0) {
                m_instances[0].focus();
                return;
            }

            var newInstance = m_initFunction.call(m_publicApp, args);
            if (!OOP.inherits(newInstance, Frame)) {
                throw new Exception("All calls to the init() function must return a Frame.");
            }
            newInstance.close(function () {
                var instanceIndex = m_instances.indexOf(newInstance);
                if (instanceIndex >= 0) {
                    m_instances.splice(instanceIndex, 1);
                }
            }).open();
            m_instances.push(newInstance);
        },

        /**
         * Gets the number of instances of this application that are currently running.
         * @public
         * @instance
         * @memberof Core.Application
         * @returns {number} The number of instances of this application currently running.
         */
        getNumberInstances: function () {
            return m_instances.length;
        },

        /**
         * Gets an instance of
         * @param index
         * @returns {*}
         */
        getInstance: function (index) {
            if (index < m_instances.length) {
                return m_instances[index];
            }
            throw new Exception("Index out of bounds!");
        }
    };

    return m_publicApp;
}