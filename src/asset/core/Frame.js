/** @namespace Core **/

(function() {
	var
        // constants (note that these are just programmatic constants; their values can be defined later as necessary)
        PANE_WIDTH_PADDING = 0,
		PANE_HEIGHT_PADDING = 25, // titlebar height
		PANE_HEIGHT_WITHSTATUS_PADDING = 38,
		LAST_FRAME_SETTING,
		FRAME_DEFAULT_SETTINGS,
		FRAME_SETTINGS_ALL_MASK,

        // variables
		activeFocusFrame = null;
    /**
     * @public
     * @name Core.FrameSettings
     * @description Settings which can be applied to [Frames]{@link Core.Frame} that manage how the Frame should
     * present itself. This is a bitwise flag enumeration, and the individual values can be bitwise AND or OR together
     * to produce a valid settings value, which Frames are able to decode. The values of this enumeration will
     * typically be represented in the following way: if their value is present in a settings variable, then the
     * setting they represent will be enabled (for example, when FrameSettings.CLOSEBUTTON is provided, a close
     * button will be displayed on the Frame. Note, however, that {@link Core.Frame.applySetting} allows for
     * passing an additional parameter which will invert the meaning of the settings variable, in order to toggle on
     * or off that particular setting. Functions which make use of this selective inversion of meaning will always
     * document that functionality.
     * @enum
     * @readonly
     */
    FrameSettings = {
        NONE:            0,
        CLOSEBUTTON:     1,
        MINIMIZE:        2,
        MAXIMIZE:        4,
        RESIZABLE:       8,
        FIXED:          16,
        STATUSBAR:      32
    };
    LAST_FRAME_SETTING = FrameSettings.STATUSBAR;
    FRAME_SETTINGS_ALL_MASK = (LAST_FRAME_SETTING * 2) - 1;
    FRAME_DEFAULT_SETTINGS = (FrameSettings.CLOSEBUTTON | FrameSettings.MINIMIZE | FrameSettings.MAXIMIZE); // jshint ignore:line

    /**
     * Constructs a new instance of a Frame.
     * @param {string} hostApplicationName - The textual handle of the [Application]{@link Core.Application} that
     * that is creating this instance of a Frame.
     * @param {Core.FrameSettings} defaultSettings - The settings used at initialisation to construct this
     * instance of the frame. Note that these are only used on initialisation and are discarded afterwards. These
     * settings may be changed dynamically during runtime by the appropriate member functions.
     * @constructor
     * @class
     * @classdesc A Frame is, simply put, a window. Frames are user interface representations of an
     * [Application]{@link Core.Application} instance -- when a new instance of an Application begins, a Frame is
     * created; when that Application instance ends, that Frame is closed. If the Frame is closed, the Application
     * instance ends. There is a symbiotic relation between Applications and Frames. Basically, however, think of
     * Frames as a full window on an operating system (that's what they are) -- they are represented on the
     * [Taskbar]{@link Core.Taskbar}; * they have titlebars which display text; they have status bars (optionally);
     * close, minimise, maximise buttons; they have [Panes]{@link Core.Pane} which are what displays the actual
     * content of the Application instance.
     *
     * For the majority of calls during an instance of the Application, you would be working with Panes, as these
     * represent the user area. Frames are the managers of the Panes -- they are responsible for the titlebars and
     * statusbars and displaying on the Taskbar, as well as managing the resizing of the applications and all operating
     * system event handling and generation.
     * @public
     * @name Core.Frame
     */
	window.Frame = function(hostApplicationName, defaultSettings) {
		var elem = $(document.createElement("div")),
			titlebarElem = $(document.createElement("div")),
			titleElem = $(document.createElement("div")),
            buttonsElem = $(document.createElement("div")),
			closeButtonElem = $(document.createElement("div")),
			minimizeButtonElem = $(document.createElement("div")),
			maximizeButtonElem = $(document.createElement("div")),
			paneElem = $(document.createElement("div")),
			statusElem = $(document.createElement("div")),
            closeCallbacks = [];
		
		elem.addClass("frame").addClass("nostatusbar");
		titlebarElem.addClass("titlebar").appendTo(elem);
		titleElem.addClass("title").appendTo(titlebarElem);
        buttonsElem.addClass("buttons").appendTo(titlebarElem);
		minimizeButtonElem.addClass("button").addClass("minimize");
		maximizeButtonElem.addClass("button").addClass("maximize");
		closeButtonElem.addClass("button").addClass("close");
		buttonsElem.append(closeButtonElem, minimizeButtonElem, maximizeButtonElem);
		paneElem.addClass("pane").appendTo(elem);
		statusElem.addClass("status").appendTo(elem);
		Workspace.stage(elem);
        
        if (hostApplicationName) {
            paneElem.addClass("application-" + hostApplicationName.toString().toLowerCase());
        } else {
            paneElem.addClass("application-none");
        }

		var framePublic = {

            /**
             * Gets whether this frame is currently open within the workspace.
             * @returns {Boolean} Whether the frame is currently open or not.
             */
			isOpen: function() {
				if (elem.parent().length === 0) {
					return false;
				}

				return elem.is("div.workspace:not(.staging) > div.frame");
			},
			open: function() {
				if (!framePublic.isOpen()) {
					Workspace.present(elem);
				}
				framePublic.focus();

				return framePublic;
			},

            /**
             * If a function callback is provided as an optional parameter, this function will register that callback
             * for when the frame is closed; the frame itself will not close if a callback is registered. If no
             * callback is provided, then this function will close the frame and all registered callbacks will be
             * triggered.
             * @param {Function} [callback] A callback that should be registered to be called when the instance of
             * this Frame is closed.
             * @returns {Frame} The current instance of the Frame.
             */
			close: function(callback) {
                if (typeof callback === "function") {
                    closeCallbacks.push(callback);
                } else {
                    framePublic.unfocus();
                    if (framePublic.isOpen()) {
                        Workspace.remove(elem);
                    }
                    for (var index = 0; index < closeCallbacks.length; ++index) {
                        closeCallbacks[index](framePublic);
                    }
                }

				return framePublic;
			},

			setTitle: function (text) {
				titleElem.text(text);
			},
			getTitle: function () {
				return titleElem.text();
			},
			setStatus: function (text) {
				statusElem.text(Util.standardizeStringEntities(text));
			},
			getStatus: function () {
				if (elem.hasClass("nostatusbar")) {
					return "";
				}
				return statusElem.text();
			},

			setSize: function(width, height) {
				if (typeof width !== "number" || typeof height !== "number") {
					throw new Exception("Width and height must both be numbers");
				}
				elem.width(width + PANE_WIDTH_PADDING);
				elem.height(height + (elem.hasClass("nostatusbar") ? PANE_HEIGHT_PADDING : PANE_HEIGHT_WITHSTATUS_PADDING));
			},
			getWidth: function() {
				return paneElem.width();
			},
			getHeight: function() {
				return paneElem.height();
			},

			setPosition: function(x, y) {
				if (typeof x === "string" && x === "CENTER") {
					// Special case for setPosition("CENTER");
					x = (Workspace.getWidth() - elem.outerWidth()) / 2;
					y = (Workspace.getHeight() - elem.outerHeight()) / 2;
				}
				else if (typeof x !== "number" || typeof y !== "number") {
					throw new Exception("X and Y must both be numeric.");
				}
				elem.offset({
					top: y,
					left: x
				});
			},
			center: function() {
				framePublic.setPosition("CENTER");
			},
			getX: function() {
				return elem.position().left;
			},
			getY: function() {
				return elem.position().top;
			},

			getPane: function() {
				return new Pane(framePublic, paneElem);
			},

			hasFocus: function () {
				return elem.hasClass("focus");
			},
			focus: function () {
				if (activeFocusFrame !== framePublic) {
					if (activeFocusFrame !== null) {
						activeFocusFrame.unfocus();
					}
					activeFocusFrame = framePublic;
					elem.addClass("focus");
					console.log("focus gained!");
				}

				return framePublic;
			},
			unfocus: function () {
				if (activeFocusFrame === framePublic) {
					elem.removeClass("focus");
					activeFocusFrame = null;
					console.log("focus lost!");
				}

				return framePublic;
			},

			getSettings: function() {
				var reconstruct = 0;

				if (!closeButtonElem.hasClass("hidden")) {
					reconstruct |= FrameSettings.CLOSEBUTTON; // jshint ignore: line
				}

				if (!minimizeButtonElem.hasClass("hidden")) {
					reconstruct |= FrameSettings.MINIMIZE; // jshint ignore: line
				}

				if (!maximizeButtonElem.hasClass("hidden")) {
					reconstruct |= FrameSettings.MAXIMIZE; // jshint ignore: line
				}

                if (!elem.draggable("option", "disabled")) {
                    reconstruct |= FrameSettings.FIXED; // jshint ignore: line
                }

				if (!elem.hasClass("nostatusbar")) {
					reconstruct |= FrameSettings.STATUSBAR; // jshint ignore: line
				}

				return reconstruct;
			},
			applySettings: function(settings) {
				if (settings & FrameSettings.CLOSEBUTTON) {  // jshint ignore: line
					if (closeButtonElem.hasClass("hidden")) {
						closeButtonElem.removeClass("hidden");
					}
				}
				else {
					if (!closeButtonElem.hasClass("hidden")) {
						closeButtonElem.addClass("hidden");
					}
				}
				if (settings & FrameSettings.MINIMIZE) {  // jshint ignore: line
					if (minimizeButtonElem.hasClass("hidden")) {
						minimizeButtonElem.removeClass("hidden");
					}
				}
				else {
					if (!minimizeButtonElem.hasClass("hidden")) {
						minimizeButtonElem.addClass("hidden");
					}
				}

				if (settings & FrameSettings.MAXIMIZE) {  // jshint ignore: line
					if (maximizeButtonElem.hasClass("hidden")) {
						maximizeButtonElem.removeClass("hidden");
					}
				}
				else {
					if (!maximizeButtonElem.hasClass("hidden")) {
						maximizeButtonElem.addClass("hidden");
					}
				}

				if (settings & FrameSettings.FIXED) {  // jshint ignore: line
					elem.draggable("disable");
				}
				else {
					elem.draggable("enable");
				}

				if (settings & FrameSettings.STATUSBAR) {  // jshint ignore: line
					if (elem.hasClass("nostatusbar")) {
						elem.removeClass("nostatusbar");
					}
				}
				else {
					if (!elem.hasClass("nostatusbar")) {
						elem.addClass("nostatusbar");
					}
				}
			},
			applySetting: function(setting, value) {
				if (value !== false) {
					this.applySettings(this.getSettings() | setting);  // jshint ignore: line
				}
				else {
					this.applySettings(this.getSettings() & (FRAME_SETTINGS_ALL_MASK - setting));  // jshint ignore: line
				}
				return this;
			}
		};
		framePublic.isPublicFor = Frame;
		
		closeButtonElem.click(function (e) {
			framePublic.close();
			e.stopImmediatePropagation();
		});
		
		elem.click(function () {
			framePublic.focus();
		}).draggable({
			handle: "div.titlebar",
			cancel: "div.titlebar > div.button",
			containment: "parent",
			cursor: "move",
			start: function () {
				framePublic.focus();
			}
		});
		
		if (typeof defaultSettings === "number") {
			framePublic.applySettings(defaultSettings);
		} else {
            framePublic.applySettings(FRAME_DEFAULT_SETTINGS);
        }
		return framePublic;
	};
})();

$(document).keyup(function (e) {
	if (e.which === 27) {
		x = new Frame();
		//x.applySetting(Frame.STATUSBAR, true);
		x.setTitle("hello world");
		x.setStatus("this is the status bar.");
		x.open();
		x.center();
	}
});