Reflection = function() {
	var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;

	return {
		getParameterNames: function(func) {
			if (typeof func != "function") {
				return [];
			}
			
			if (func.name == "servFuncIntCallbackFunc") {
				return func.paramNames;
			}
			
			var funcStr = func.toString().replace(STRIP_COMMENTS, ''),
				result = funcStr.slice(funcStr.indexOf('(') + 1, funcStr.indexOf(')')).match(/([^\s,]+)/g);;
				
			if (result === null) {
				return [];
			}
			
			return result;
		}
	};
}();