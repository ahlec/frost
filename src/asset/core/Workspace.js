Workspace = (function() {
	var workspaceElem = $(document.createElement("div")),
	    stagingWorkspaceElem = $(document.createElement("div")),
	    taskbarElem = $(document.createElement("div"));
	
	workspaceElem.addClass("workspace");
	stagingWorkspaceElem.addClass("workspace").addClass("staging");
	taskbarElem.addClass("taskbar");
	$(document).ready(function () {
		$(document.body).append(workspaceElem, stagingWorkspaceElem, taskbarElem);
	});

	$(window).resize(function () {
		var workspaceChildren = workspaceElem.children(),
		    stagingChildren = stagingWorkspaceElem.children(),
		    workspaceHeight = workspaceElem.height(),
		    workspaceWidth = workspaceElem.width(),
		    child,
		    childOffset,
		    childWidth,
		    childHeight;

		for (var index = 0; index < workspaceChildren.length; ++index) {
			child = $(workspaceChildren[index]);
			childOffset = child.position();
			childWidth = child.width() - 3;
			childHeight = child.height() - 3;
			if (childOffset.top + childHeight > workspaceHeight) {
				child.animate({
					top: "-=" + (childOffset.top + childHeight - workspaceHeight) + "px"
				}, 0);
			}
			if (childOffset.left + childWidth > workspaceWidth) {
				child.animate({
					left: "-=" + (childOffset.left + childWidth - workspaceWidth) + "px"
				}, 0);
			}
		}

		for (var index = 0; index < stagingChildren .length; ++index) {
			child = $(stagingChildren[index]);
			childOffset = child.position();
			childWidth = child.width() - 3;
			childHeight = child.height() - 3;
			if (childOffset.top + childHeight > workspaceHeight) {
				child.animate({
					top: "-=" + (childOffset.top + childHeight - workspaceHeight) + "px"
				}, 0);
			}
			if (childOffset.left + childWidth > workspaceWidth) {
				child.animate({
					left: "-=" + (childOffset.left + childWidth - workspaceWidth) + "px"
				}, 0);
			}
		}
	});
	
	return {
		present: function (elem) {
			if (!Util.isDomElement(elem)) {
				throw new Exception("Only DOM elements may be used with Workspace.show().");
			}

			elem.detach();
			workspaceElem.append(elem);
		},

		stage: function (elem) {
			if (!Util.isDomElement(elem)) {
				throw new Exception("Only DOM elements may be used with Workspace.hide().");
			}

			elem.detach();
			stagingWorkspaceElem.append(elem);
		},
		
		remove: function (elem) {
			elem.detach();
		},

		showTaskbar: function () {
			if ($(document.body).hasClass("notaskbar")) {
				$(document.body).removeClass("notaskbar");
				$(window).resize();
			}
		},
		hideTaskbar: function () {
			if (!$(document.body).hasClass("notaskbar")) {
				$(document.body).addClass("notaskbar");
				$(window).resize();
			}
		},
		
		getWidth: function () {
			return workspaceElem.width();
		},
		getHeight: function () {
			return workspaceElem.height();
		}
	};
})();