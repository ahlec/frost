function Entity(guid) {
	var name;
	
	this.getGuid = function() {
		return guid;
	},
	
	this.isDirectory = function() {
		return false;
	},
	
	this.getName = function() {
		return name;
	},
	
	this.getParent = function() {
	},
	
	this.setName = function(newName) {
		name = newName;
	}
}