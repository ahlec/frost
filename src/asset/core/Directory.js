function Directory (guid) {
	extend.call(this, Entity, guid);

	this.isDirectory = function() {
		return true;
	},
	
	this.setName = function(newName) {
		this.base.setName("DIR '" + newName + "'");
	}
}