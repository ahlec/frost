function Form(submitFunction) {
	if (typeof submitFunction != "function") {
		throw new Exception("Must provide a function for form submission.");
	}
	var formElem = $(document.createElement("form")),
		submitFunctionParams = Reflection.getParameterNames(submitFunction),
		beforeSubmitHandlers = new Array(),
		processHandlers = new Array(),
		isSubmitting = false;
		
	formElem.submit(function (e) {
		e.preventDefault();
		if (isSubmitting) {
			return false;
		}
		
		console.log("submitting");
		
		// Process the beforeSubmit handlers
		if (beforeSubmitHandlers.length > 0) {
			for (var index = 0; index < beforeSubmitHandlers.length; ++index) {
				if (!beforeSubmitHandlers[index].call(formElem)) {
					return false;
				}
			}
		}
	
		// Prepare the form for submission
		ifSubmitting = true;
		var submitArgs = [],
			paramValArray;
		for (var index = 0; index < submitFunctionParams.length; ++index) {
			paramValArray = formElem.find("input[name=" + submitFunctionParams[index] + "]");
			if (paramValArray.length == 0) {
				submitArgs.push(null);
			}
			else {
				submitArgs.push(paramValArray.val());
			}
		}
		
		// Process the form function
		var result = submitFunction.apply(this, submitArgs);
	
		if (processHandlers.length > 0) {
			for (var index = 0; index < processHandlers.length; ++index) {
				processHandlers[index].call(formElem, result);
			}
		}
	
		// Finish up
		return false;
	});
	
	formElem.process = function(callback) {
		if (typeof callback == "function") {
			processHandlers.push(callback);
		}
	};
	
	var formPublic = {
		addInput: function (settings) {
			if (typeof settings != "object") {
				throw new Exception("Settings must be an object when passed to addInput().");
			}
			var inputType = (settings.hasOwnProperty("type") ? settings.type.toLowerCase() : "text"),
				isTextarea = (inputType == "textarea"),
				labelElem = $(document.createElement("label")),
				inputElem = $(document.createElement(isTextarea ? "textarea" : "input")),
				generatedId = Util.newDomId();

			if (settings.hasOwnProperty("label")) {
				labelElem.attr("for", generatedId).text(settings.label);
				formElem.append(labelElem);
			}

			inputElem.attr("id", generatedId);
			if (!isTextarea) {
				inputElem.attr("type", inputType);
			}
			if (!settings.hasOwnProperty("name")) {
				throw new Exception("Settings must define a name in order to create an input via addInput().");
			}
			inputElem.attr("name", settings.name);

			formElem.append(inputElem);
		},
		addSubmitButton: function (settings) {
			if (typeof settings != "object") {
				throw new Exception("Settings must be an object when passed to addSubmitButton().");
			}
			var inputElem = $(document.createElement("input")),
				generatedId = Util.newDomId();
			
			inputElem.attr("type", "submit");
			
			if (settings.hasOwnProperty("value")) {
				inputElem.val(settings["value"]);
			}
			
			formElem.append(inputElem);
		}
	};
	formPublic.isPublicFor = Form;
	formPublic.domElement = formElem;

	return formPublic;
}