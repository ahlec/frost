Login = null;

(function () {

	function loginInit() {
		var frame = new Frame("login", FrameSettings.FIXED),
			frameForm = new Form(Auth.login),
			username = frameForm.addInput({
				name: "username",
				label: "Username",
				type: "text"
			}),
			password = frameForm.addInput({
				name: "password",
				label: "Password",
				type: "password"
			}),
			submitButton = frameForm.addSubmitButton({
				value: "Login"
			}),
            forgotPasswordLink = $(document.createElement("a"));
		
		frame.setTitle("Login");
        frame.setSize(400, 218);
		frame.center();
		
        forgotPasswordLink.text("Forgot your password?").addClass("forgot-password").click(function () {
            alert("feature coming... someday? (honestly not a very important feature in the larger scheme of things in frOSt (for now)");
        });
        
		frame.getPane().append(frameForm, forgotPasswordLink);
		return frame;
	}
	
	/** LOGIN **/
	Login = new Application({
        isSingleInstance: true,
		init: loginInit
	});
})();

/* STARTUP */
$(document).ready(function () {
	if (!Auth.isLoggedIn()) {
		Login.call();
	}
});