<?php
if (!ob_start())
{
	define ("OUTPUT_BUFFER_NOT_STARTED", true);
	trigger_error("Unable to start the output buffer.", E_USER_ERROR);
}
define ("SERVER_OUTPUT_ERROR", 0);
define ("SERVER_OUTPUT_SUCCESS", 1);

function serverOutput($flag, $message = null, $data = null)
{
	$outputArr = array();
	if ($flag == SERVER_OUTPUT_ERROR)
	{
		$outputArr["status"] = "error";
	}
	else if ($flag == SERVER_OUTPUT_SUCCESS)
	{
		$outputArr["status"] = "success";
	}
	else
	{
		$outputArr["servErr"] = true;
		$outputArr["status"] = "error";
		$outputArr["servErrMessage"] = "Unspecified output flag used.";
		$outputArr["servErrData"] = array("flag" => $flag);
	}
	
	if ($message !== null)
	{
		$outputArr["message"] = strval($message);
	}
	if ($data !== null)
	{
		$outputArr["data"] = $data;
	}

	if (!defined("OUTPUT_BUFFER_NOT_STARTED"))
	{
		$outputBuffer = ob_get_contents();
		if ($outputBuffer !== false && mb_strlen($outputBuffer) > 0)
		{
			$outputArr["buffer"] = $outputBuffer;
		}
		
		if (!ob_end_clean())
		{
			trigger_error("Unable to end the output buffer.", E_USER_ERROR);
		}
	}
	
	exit(json_encode($outputArr));
}

function outputError($message = null, $data = null)
{
	serverOutput(SERVER_OUTPUT_ERROR, $message, $data);
}

function outputSuccess($message = null, $data = null)
{
	serverOutput(SERVER_OUTPUT_SUCCESS, $message, $data);
}
?>