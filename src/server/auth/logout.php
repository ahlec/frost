<?php
require_once ("isLoggedIn.php");

function logout()
{
	if (!isLoggedIn()) {
		return array("success" => false, "message" => "NOT_LOGGED_IN");
	}
	
	unset($_SESSION[FROST_AUTH_SESSION_NAME]);
	return array("success" => true);
}
?>