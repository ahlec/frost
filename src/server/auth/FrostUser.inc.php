<?php

require_once (dirname(__FILE__) . "/../../config.inc.php");
require_once (FROST_DOCUMENT_ROOT . "/server/FrostClass.inc.php");

class FrostUser extends FrostClass {
    public $user_id;
    public $username;
    private $user_hash;
    
    private function __construct($fnc_results) {
        $this->user_id = $fnc_results["user_id"];
        $this->user_hash = $fnc_results["user_hash"];
        $this->username = $fnc_results["username"];
    }
    
    public static function get($database, $user_id) {
        if ($database == null || $user_id == null) {
            return null;
        }
        if (!is_numeric($user_id)) {
            return null;
        }
        
        $db_values = $database->query("CALL fnc_auth_get_user_by_id(" . $user_id . ");");
        if ($db_values === false || $db_values->getNumberOfRows() != 1) {
            return null;
        }
        return new FrostUser($db_values->fetchArray());
    }
};

?>