<?php
require_once ("isLoggedIn.php");

function login($username, $password)
{
	if (isLoggedIn()) {
		return array("success" => false, "message" => "ALREADY_LOGGED_IN");
	}
	
	$database = frOSt\openDatabase();
	$procResults = $database->querySingle("SELECT fnc_processLogin('" . $database->escapeString($username) .
		"','" . md5($password) . "');");
	
	if ($procResults === null)
	{
		return array("success" => false, "message" => "INVALID_CREDENTIALS");
	}
	
	$_SESSION[FROST_AUTH_SESSION_NAME] = $procResults;
	return array("success" => true);
}
?>