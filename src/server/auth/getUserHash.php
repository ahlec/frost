<?php
require_once ("isLoggedIn.php");

function getUserHash()
{
	if (!isLoggedIn())
	{
		return null;
	}
	
	return $_SESSION[FROST_AUTH_SESSION_NAME];
}
?>