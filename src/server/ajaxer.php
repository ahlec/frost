<?php
require_once (dirname(__FILE__) . "/include.inc.php");
require_once (dirname(__FILE__) . "/output.inc.php");

// Error handling
function _serverErrorHandler($errorNo, $errorStr, $errorFile, $errorLine)
{
	if (!(error_reporting() & $errorNo))
	{
        // This error code is not included in error_reporting
        return;
    }
	
	if ($errorNo == E_USER_ERROR)
	{
		outputError($errorStr);
		exit();
	}
	
	return true;
}
set_error_handler("_serverErrorHandler");


$filepath = $_GET["filepath"];
if (!isset($filepath))
{
	outputError("Must specify a location within the server to execute this function.");
}

// Ensure that the destination is within the server
$absoluteFilepath = realpath(dirname(__FILE__) . "/" . $filepath);
if ($absoluteFilepath === false)
{
	outputError("The specified function could not be found within the server.");
}
if (mb_strpos($absoluteFilepath, dirname(__FILE__)) !== 0)
{
	outputError("The specified function does not exist within the server. Security access denied.");
}

// Ensure that the file is a PHP file only (only '.php')
$absoluteFilename = basename($absoluteFilepath);
$absoluteExtensionIndex = mb_strpos($absoluteFilename, ".");
if ($absoluteExtensionIndex === false)
{
	outputError("The specified function is not a valid function within the server.");
}
$absoluteExtension = mb_substr($absoluteFilename, $absoluteExtensionIndex);
if ($absoluteExtension !== ".php")
{
	outputError("The specified function is not actually a function within the server.");
}

// Include the specified PHP file
require_once ($absoluteFilepath);

// Parse any flags given to the function
$functionFlags = array();
if (isset($_POST["incFlags"]) || isset($_GET["incFlags"]))
{
	$flagKeys = explode(",", (isset($_POST["incFlags"]) ? $_POST["incFlags"] : $_GET["incFlags"]));
	foreach ($flagKeys as $flag)
	{
		if (isset($_POST["flag_" . $flag]))
		{
			$functionFlags[$flag] = $_POST["flag_" . $flag];
			unset($_POST["flag_" . $flag]);
		}
		else if (isset($_GET["flag_" . $flag]))
		{
			$functionFlags[$flag] = $_GET["flag_ " . $flag];
			unset($_GET["flag_" . $flag]);
		}
	}
}

// Determine the function and run it
$absoluteFunctionBindingName = mb_substr($absoluteFilename, 0, $absoluteExtensionIndex);
if (!function_exists($absoluteFunctionBindingName))
{
	outputError("The specified function is not valid to be called directly. Access denied.");
}

$reflectionFunc = new ReflectionFunction($absoluteFunctionBindingName);
$funcArguments = array();
$funcArgument = null;
foreach ($reflectionFunc->getParameters() as $reflectionParam)
{
	if ($reflectionParam->getName() == "flags")
	{
		$funcArgument = $functionFlags;
	}
	else if (isset($_POST[$reflectionParam->getName()]))
	{
		$funcArgument = $_POST[$reflectionParam->getName()];
	}
	else if (isset($_GET[$reflectionParam->getName()]))
	{
		$funcArgument = $_GET[$reflectionParam->getName()];
	}
	else if (isset($_FILES[$reflectionParam->getName()]))
	{
		$funcArgument = $_FILES[$reflectionParam->getName()];
	}
	else
	{
		if ($reflectionParam->isOptional())
		{
			$funcArgument = $reflectionParam->getDefaultValue();
		}
		else
		{
			outputError("The parameter '" . $reflectionParam->getName() . "' is required.");
		}
	}
	
	if ($reflectionParam->isArray())
	{
		if (!is_array($funcArgument))
		{
			$funcArgument = array($funcArgument);
		}
	}
	else
	{
		if (is_array($funcArgument))
		{
			if (count($funcArgument) == 1)
			{
				$funcArgument = reset($funcArgument);
			}
			else
			{
				outputError("The parameter '" . $reflectionParam->getName() . "' is not an array, and more than one value was passed.");
			}
		}
	}
	
	$funcArguments[] = $funcArgument;
}

$functionResult = $reflectionFunc->invokeArgs($funcArguments);
outputSuccess(null, $functionResult);
?>