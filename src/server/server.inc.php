<?php

// Include and setup
define ("FROST_SERVER_FILE", "SERVER_FILE");
require_once (dirname(__FILE__) . "/../config.inc.php");
require_once (dirname(__FILE__) . "/output.inc.php");

// Error handling
function _serverErrorHandler($errorNo, $errorStr, $errorFile, $errorLine)
{
	if (!(error_reporting() & $errorNo))
	{
        // This error code is not included in error_reporting
        return;
    }
	
	if ($errorNo == E_USER_ERROR)
	{
		outputError($errorStr);
		exit();
	}
	
	return true;
}
set_error_handler("_serverErrorHandler");
?>