<?php
require_once (dirname(__FILE__) . "/../../config.inc.php");

if (!isset($_SESSION[FROST_SESSION_HASH]))
{
    // Hash generation code doesn't need to be strong or anything; we just need something random, and this looks like it'd
    // be good. The generation code itself was taken from http://stackoverflow.com/questions/2293684/what-is-the-best-way-to-create-a-random-hash-string.
    // Note that if the server running this code isn't able to support either of these functions (if it's even possible to not
    // support them), there shouldn't be any problem with using other means to generate a random string, because that's all
    // we really need; something that provides us with a certain degree of randomness that we can use to bind between the client
    // and the server.
    $_SESSION[FROST_SESSION_HASH] = array(
        "hash" => bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)),
        "generated" => time(),
        "lastRenewal" => time()
    );
}

?>