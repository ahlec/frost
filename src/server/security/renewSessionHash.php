<?php
require_once("init.inc.php");

// Returns: (boolean) true if the session hash was renewed, false if it had already expired or could not be renewed.
function renewSessionHash()
{
    if (!isset($_SESSION[FROST_SESSION_HASH]))
    {
        echo "nooooo";
        return false;
    }
    
    if ($_SESSION[FROST_SESSION_HASH]["lastRenewal"] + FROST_SESSION_LIFETIME_DURATION < time())
    {
        unset($_SESSION[FROST_SESSION_HASH]);
        echo "expired!!";
        return false;
    }
    
    $_SESSION[FROST_SESSION_HASH]["lastRenewal"] = time();
    return true;
}

?>