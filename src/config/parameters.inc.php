<?php
// Interpret flags
define ("INTERP_NOFLAGS", 0);
define ("INTERP_STRICT", 1);
define ("INTERP_POSITIVE", 2);
define ("INTERP_NOTNULL", 4);
define ("INTERP_NOTWHITESPACE", 8);
define ("INTERP_OPTIONAL", 16);
define ("INTERP_POST", 32);
define ("INTERP_RAW", 64);
define ("INTERP_EMPTY_STR_NOT_NULL", 128);


// Utility function
function _interpretTranslate($variable, $flags, $default)
{
	$exists = false;
	if ($flags & INTERP_POST)
	{
		$exists = isset($_POST[$variable]);
		$var = $_POST[$variable];
	}
	else if ($flags & INTERP_RAW)
	{
		$var = $variable;
		$exists = true;
	}
	else
	{
		$exists = isset($_GET[$variable]);
		$var = $_GET[$variable];
	}
	
	if (!$exists)
	{
		if (($flags & INTERP_OPTIONAL) != INTERP_OPTIONAL)
		{
			_interpretErr("%VAR% is a required parameter.", $variable, $flags);
		}
		
		return $default;
	}
	
	return mb_strtolower($var);
}
function _interpretErr($message, $variable, $flags)
{
	$msg = preg_replace("/%VAR%/", $variable, $message);
	trigger_error($msg, E_USER_ERROR);
}

// Interpret functions
function interpretBool($variable, $flags = INTERP_NOFLAGS, $default = false)
{
	$value = _interpretTranslate($variable, $flags, $default);
	
	if (($flags & INTERP_STRICT) != INTERP_STRICT)
	{
		if (ctype_digit($value))
		{
			if (intval($value) === 0)
			{
				return false;
			}
			else if (intval($value) === 1)
			{
				return true;
			}
		}
		else
		{
			if ($value == "yes" || $value == "y")
			{
				return true;
			}
			else if ($value == "no" || $value == "n")
			{
				return false;
			}
		}
	}
	
	if ($value == "true" || $value === true)
	{
		return true;
	}
	else if ($value == "false" || $value === false)
	{
		return false;
	}
	
	trigger_error("Value '" . htmlentities($variable) . "' could not be interpretted as a boolean.", E_USER_ERROR);
}

function interpretInt($variable, $flags = INTERP_NOFLAGS, $default = 0)
{
	$value = _interpretTranslate($variable, $flags, $default);
	
	if (!is_numeric($value))
	{
		trigger_error("Value '" . $variable . "' is not a numeric value.", E_USER_ERROR);
	}
	
	if (($flags & INTERP_STRICT) == INTERP_STRICT)
	{
		if (preg_match("/^(\\+|\\-)?[0-9]+$/m", $value) !== 1)
		{
			trigger_error("Value '" . $variable . "' is numeric, but is not a base10 integer.", E_USER_ERROR);
		}
		
		$intValue = intval($value, 10);
	}
	else
	{
		$intValue = intval($value, 0);
	}
	
	if (($flags & INTERP_POSITIVE) == INTERP_POSITIVE && $intValue < 0)
	{
		trigger_error("Value '" . $variable . "' is a negative number.", E_USER_ERROR);
	}
	
	return $intValue;
}

define ("INTERP_STRING_DEFAULT_FLAGS", (INTERP_NOTNULL | INTERP_NOTWHITESPACE));
function interpretString($variable, $flags = INTERP_STRING_DEFAULT_FLAGS, $default = null)
{
	$value = _interpretTranslate($variable, $flags, $default);
	if (mb_strlen($value) == 0 && ($flags & INTERP_EMPTY_STR_NOT_NULL) != INTERP_EMPTY_STR_NOT_NULL)
	{
		$value = null;
	}
	$value = strval($value);
	
	if ($flags & INTERP_NOTNULL && $value == null)
	{
		_interpretErr("%VAR% may not be null.", $variable, $flags);
	}
	
	if ($flags & INTERP_NOTWHITESPACE && preg_match("/[^\\s]/", $value) !== 1)
	{
		_interpretErr("%VAR% may not be composed only of whitespace.", $variable, $flags);
	}
	
	return $value;
}
?>