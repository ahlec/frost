#!/bin/sh
# this should be passed two arguments: "source directory" and "build directory"

src_path=$1
build_path=$2
length_of_src_path=$((${#src_path} + 1))
#length is the length of the source path plus the trailing forward slash

compile_file() {
    #no_source_dir will take 'src/a/b/c.FILE' to 'a/b/c.FILE'
    no_source_dir=$(echo $1 | cut -c ${length_of_src_path}-)

    #build_path will take 'a/b/c.FILE' to 'build/a/b/c.FILE'
    compile_path="${build_path}${no_source_dir}"
    
    # make sure the directory path exists in the build folder
    mkdir -p $(dirname "${compile_path}")
    
    no_compile_message=0
    filename=$(basename $1)
    if [ ${filename:0:1} == '_' ]; then
        no_compile_message=1
        echo "skipping: $i (underscore prefix)"
    else
        #### COMPILE ####
        if [ ${1: -5} == '.scss' ]; then
            # change file extensions
            compile_path="${compile_path%.*}.css"
        
            sass --no-cache --sourcemap=none $i $compile_path
        else
            # No special operations. Just copy.
            cp $1 $compile_path
        fi
    fi
    
    if [ $no_compile_message == 0 ]; then
        compile_return_code=$?;
        if [ $compile_return_code == 0 ]; then
            echo "compiled: $1 => $compile_path"
        else
            echo "failed: $1 => $compile_path"
            exit $compile_return_code
        fi
    fi
}

recurse() {
 for i in "$1"/*;do
    if [ -d "$i" ];then
        if [[ -e "$i/.htaccess" ]]; then
            compile_file "$i/.htaccess"
        fi
        recurse "$i" "$2"
    elif [ -f "$i" ]; then
        compile_file $i
    fi
 done
}

if [[ -e "$src_path/.htaccess" ]]; then
    compile_file "$src_path/.htaccess"
fi
recurse $src_path