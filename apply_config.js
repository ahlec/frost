// Validate to make sure that we've got the right data
if (process.argv.length < 4) {
    console.error("You must provide at least two arguments to this function: SOURCE_DIR and BUILD_DIR. Additionally, a third argument for config file may be provided.");
}
var sourceDirectory = process.argv[2],
    buildDirectory = process.argv[3];

// Open modules
var fs = require('fs');

// Open the config file
var configFilename,
    config;

if (process.argv.length < 5) {
    configFilename = 'config.json';
} else {
    configFilename = process.argv[4];
}
config = require('./' + configFilename);
if (typeof config !== 'object') {
    console.error("Could not find the configuration file '" + configFilename + "'.");
}

// Define a function to apply the master site configuration JSON file to an arbitrary file in SOURCE and save it to BUILD_DIR
function replaceAll(str, oldSubstr, newSubstr) {
    var temp = str;
    while (temp.indexOf(oldSubstr) >= 0) {
        temp = temp.replace(oldSubstr, newSubstr);
    }
    return temp;
}

function applyConfig(relativeFilename) {    // ie, "config.inc.php" ==> SOURCE/"config.inc.php"
    var appliedConfig = fs.readFileSync("./" + sourceDirectory + "/" + relativeFilename, { "encoding": "utf8" });
    
    function applyConfigProperty(propertyName, value) {
        if (typeof value !== 'object') {
            appliedConfig = replaceAll(appliedConfig, "{{{CONFIG:" + propertyName + "}}}", value.toString());
        } else {
            for (var subproperty in value) {
                applyConfigProperty(propertyName + (propertyName !== "" ? "." : "") + subproperty, value[subproperty]);
            }
        }
    }
    
    applyConfigProperty("", config);
    
    fs.writeFileSync("./" + buildDirectory + "/" + relativeFilename, appliedConfig);
    console.log("applied config: " + sourceDirectory + "/" + relativeFilename + " => " + buildDirectory + "/" + relativeFilename);
}

// Apply the configurations
applyConfig("config.inc.php");
applyConfig(".htaccess");
applyConfig("server/.htaccess");